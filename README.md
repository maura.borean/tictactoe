# TicTacToe 2.0

## Getting Started
This project was developed using Test Driven Development (TDD). The project is divided in two modules that represents the core and the implementation of the game. The *core* contains all the classes and interfaces necessary to create a *TicTacToe* game, and in the implementation module are all the classes needed to create a console delivery of the *2.0* version.

Some of the design patterns used in the development of the projects are: Dependency injection, Strategy and Template.
 
The following instructions will allow you to install and play the Tic Tac Toe 2.0 on your computer.

### About the game
Tic tac toe 2.0 represents an enhanced version of the classical Tic Tac Toe game. The new features are:
- You can customize the board size with a value between 3x3 and 10x10.
- You can customize each player symbol.
- There are 3 players instead of 2. One of which is controlled by an AI.
- The order the players play is randomized at the start of the game.
- Players input are expected with the pattern: X, Y.
 
### Prerequisites
Please, make sure you have installed the following tools:
- Java JDK 8
- Maven 3.3

### Installing the game

Download the project
```
git clone https://gitlab.com/maura.borean/tictactoe.git
```

Go to the folder that contains the game
```
cd tictactoe
```

Let's install the game
```
mvn install
```

You can optionally go to the `configuration.properties` file and customize your game. The file is in the path:
```
cd consoleimplementation\src\main\resources
```

And now, we can run our game! Make sure you are on the original directory before executing the next command. 
```
mvn exec:java -q -pl consoleimplementation 
```

## Authors
Maura Borean
