package com.tictactoe.player.impl;

import com.tictactoe.board.Board;
import com.tictactoe.game.Coordinate;
import com.tictactoe.player.Player;
import com.tictactoe.reader.InputHandler;
import com.tictactoe.writer.Writer;

/**
 * User representation of a {@link Player}.
 * 
 * @author Maura Borean
 *
 */
public class HumanPlayer implements Player {

    private char id;
    private InputHandler inputHandler;
    private Writer writer;

    public HumanPlayer(char id, InputHandler inputHandler, Writer writer) {
	this.id = id;
	this.inputHandler = inputHandler;
	this.writer = writer;
    }

    @Override
    public char getId() {
	return id;
    }

    @Override
    public Coordinate getPlay() {
	return inputHandler.getPlayerCoodinate(id);
    }

    @Override
    public void refreshBoard(Board board) {
	writer.printBoard(board);
    }
}
