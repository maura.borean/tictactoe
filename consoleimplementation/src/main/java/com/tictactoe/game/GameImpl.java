package com.tictactoe.game;

import com.tictactoe.board.Board;
import com.tictactoe.condition.GameOverCondition;
import com.tictactoe.condition.WinConditionHandler;
import com.tictactoe.game.Game;
import com.tictactoe.player.PlayersHandler;
import com.tictactoe.reader.ConfigurationReader;
import com.tictactoe.writer.Writer;

public class GameImpl extends Game {

    private static final int MIN_BOARD_SIZE = 3;
    private static final int MAX_BOARD_SIZE = 10;

    public GameImpl(ConfigurationReader configurationReader, WinConditionHandler winCondition, GameOverCondition gameOverCondition, PlayersHandler playerHandler, Writer writer) {
	super(getBoard(configurationReader), winCondition, gameOverCondition, playerHandler, writer);
    }

    private static Board getBoard(ConfigurationReader configurationReader) {
	int boardSize = configurationReader.getBoardSize();
	return new Board(boardSize, MIN_BOARD_SIZE, MAX_BOARD_SIZE);
    }
}
