package com.tictactoe.reader.impl;

import com.tictactoe.game.Coordinate;
import com.tictactoe.reader.InputHandler;
import com.tictactoe.reader.InputReader;
import com.tictactoe.validator.InputValidator;

public class InputHandlerImpl implements InputHandler {

    private InputReader inputReader;
    private InputValidator inputValidator;

    public InputHandlerImpl(InputReader inputReader, InputValidator inputValidator) {
	this.inputReader = inputReader;
	this.inputValidator = inputValidator;
    }

    @Override
    public Coordinate getPlayerCoodinate(char playerId) {
	String playerInput = inputReader.getInput(playerId);
	inputValidator.validatePlayerInput(playerInput);
	return buildCoordinate(playerInput);
    }

    private Coordinate buildCoordinate(String input) {
	String[] inputArr = input.split(",");

	int x = Integer.parseInt(inputArr[0].trim());
	int y = Integer.parseInt(inputArr[1].trim());

	return new Coordinate(x, y);
    }
}
