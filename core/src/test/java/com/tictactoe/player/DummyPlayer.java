package com.tictactoe.player;

import java.util.Random;

import com.tictactoe.board.Board;
import com.tictactoe.game.Coordinate;
import com.tictactoe.player.Player;

public class DummyPlayer implements Player {

    private char id;

    public DummyPlayer() {
	this.id = (char) new Random().nextInt(27);
    }

    public DummyPlayer(char id) {
	super();
	this.id = id;
    }

    public char getId() {
	return id;
    }

    @Override
    public Coordinate getPlay() {
	return new Coordinate(0, 0);
    }

    @Override
    public void refreshBoard(Board board) {
	/*
	 * Dummy refresh
	 */
    }
}
