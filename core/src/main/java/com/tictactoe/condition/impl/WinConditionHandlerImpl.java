package com.tictactoe.condition.impl;

import java.util.List;
import java.util.Optional;

import com.tictactoe.board.Board;
import com.tictactoe.condition.WinCondition;
import com.tictactoe.condition.WinConditionHandler;
import com.tictactoe.game.Move;

public class WinConditionHandlerImpl implements WinConditionHandler {

    private List<WinCondition> winConditions;

    public WinConditionHandlerImpl(List<WinCondition> winConditions) {
	this.winConditions = winConditions;
    }

    @Override
    public boolean isWin(Board board, Move move) {
	Optional<WinCondition> optWin = winConditions.stream().filter(w -> w.isPlayerWinner(board, move)).findFirst();
	return optWin.isPresent();
    }

}
